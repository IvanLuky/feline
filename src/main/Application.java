package main;

import data.Cat;
import data.FelineInterface;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class Application {
    //private static List<String> catNameList = new ArrayList<>();
    private static List<FelineInterface> cats = new ArrayList<>();

    public static void main(String[] args) {
        initGUI();
    }
    public static void initGUI(){
        JButton saveBtn = new JButton("SAVE");
        JTextField idImput = new JTextField();
        JTextField nameImput = new JTextField();
        JTextField raceImput = new JTextField();
        JTextField yearImput = new JTextField();
        JFrame window = new JFrame("Cat DataBase App");

        window.getContentPane().add(idImput);
        window.getContentPane().add(nameImput);
        window.getContentPane().add(raceImput);
        window.getContentPane().add(yearImput);
        window.getContentPane().add(saveBtn);
        window.getContentPane().setLayout(new BoxLayout(window.getContentPane(),BoxLayout.PAGE_AXIS));
        window.setBounds(100,100,400,300);
        window.show();

        saveBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    if(idImput != null && nameImput.getText() != null && raceImput.getText() != null && yearImput.getText() != null) {
                        cats.add(new Cat(Integer.valueOf(idImput.getText()),nameImput.getText(), raceImput.getText(), Integer.valueOf(yearImput.getText())));
                        System.out.println(cats.get(cats.size() - 1));
                        System.out.println("Cats in the collection " + cats.size());
                        printList();
                    }
                    idImput.setText(null);
                    nameImput.setText(null);
                    raceImput.setText(null);
                    yearImput.setText(null);
                }
                catch (NumberFormatException er){
                    System.err.println("Fill correctly spaces");
                }
            }
        });
    }
    public static void printList(){
        for (int i = 0; i < cats.size(); i++) {
            System.out.println(cats.get(i));
        }
    }
}
