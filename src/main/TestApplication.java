package main;

import data.FelineInterface;
import persistance.FelineRepository;
import persistance.FelineRepositoryInterface;


public class TestApplication {
    public static void main(String [] args){
        FelineRepositoryInterface fr = new FelineRepository();
        ((FelineRepository)fr).generate();

        for (FelineInterface f : fr.getAll()) {
            System.out.println(f);
        }
        System.out.println(fr.findId(2));
        FelineInterface cat = fr.findId(2);
        cat.setName("New name");
        System.out.println(cat);
        System.out.println();
        for (FelineInterface f : fr.getAll()) {
            System.out.println(f);
        }
    }
}
