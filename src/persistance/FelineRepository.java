package persistance;

import data.Cat;
import data.FelineInterface;
import java.util.ArrayList;
import java.util.List;

public class FelineRepository implements FelineRepositoryInterface{
    //STORAGE
    private List<FelineInterface> felines;
    public FelineRepository(){
        felines = new ArrayList<>();
    }

    @Override
    public boolean save(FelineInterface feline) {
        return false;
    }

    @Override
    public boolean delete(FelineInterface feline) {
        return false;
    }

    @Override
    public boolean update(FelineInterface feline) {
        for (int i = 0; i < felines.size(); i++) {
            if (feline.getId() == felines.get(i).getId()){
                felines.add(i,feline);
                return true;
            }
        }
        return false;
    }

    @Override
    public List<FelineInterface> getAll() {
        List<FelineInterface> clonedFeline = new ArrayList<>();
        for (FelineInterface f: felines) {
            clonedFeline.add(f.clone());
        }
        return clonedFeline;
    }

    @Override
    public FelineInterface findId(Integer id) {
        for (FelineInterface f: felines) {
            if (f.getId() == id){
                return f.clone();
            }
        }
        return null;
    }
    @Override
    public List<FelineInterface> findByName(String name) {
        List<FelineInterface> fl = new ArrayList<>();
        for ( FelineInterface f: felines) {
            if (f.getName() == name){
                fl.add(f.clone());
            }
        }
        return fl;
    }

    @Override
    public List<FelineInterface> findByRace(String race) {
        List<FelineInterface> fl = new ArrayList<>();
        for (FelineInterface f: felines) {
            if (f.getRace() == race){
                fl.add(f.clone());
            }
        }
        return fl;
    }

    //FOR TESTING
    //FILL THE LIST WITH DATA
    public void generate(){
        felines.add(new Cat(1,"Lala","Siamez",2));
        felines.add(new Cat(2,"Anfisa","BotLung",3));
        felines.add(new Cat(3,"Morda","Pufoasa",3));
    }
}
