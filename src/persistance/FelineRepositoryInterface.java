package persistance;

import data.FelineInterface;
import java.util.List;

public interface FelineRepositoryInterface{
    public boolean save(FelineInterface feline);
    public boolean delete(FelineInterface feline);
    public boolean update(FelineInterface feline);
    public List<FelineInterface> getAll();
    public FelineInterface findId(Integer id);
    public List<FelineInterface> findByName(String name);
    public List<FelineInterface> findByRace(String race);
}
