package data;

public class Cat implements FelineInterface{
    private Integer id;
    private String name;
    private String race;
    private int year;
    public Cat(Integer id,String name, String race, int year) {
        this.id = id;
        this.name = name;
        this.race = race;
        this.year = year;
    }
        @Override
    public String toString(){
        return " cat {id = "+getId()+", name = "+name + ", race = "+race + ", year = "+year+"}";
        }
        @Override
        public Cat clone(){
        return new Cat(this.id,this.name,this.race,this.year);
        }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setRace(String race) {
        this.race = race;
    }

    @Override // citim rasa
    public String getRace() {
        return race;
    }

    @Override
    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public int getYear() {
        return year;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public Integer getId() {
        return id;
    }
}
