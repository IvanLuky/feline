package data;

public interface FelineInterface extends Cloneable{
    public void setName(String name);
    public String getName();
    public void setRace(String race);
    public String getRace();
    public void setYear(int year);
    public int getYear();
    public void setId(Integer id);
    public Integer getId();
    public FelineInterface clone();
}
